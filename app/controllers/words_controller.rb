class WordsController < ApplicationController
  def show
  	@dictionary = Dictionary.find_or_create(params[:word])
  	render json: @dictionary.to_json, status: :ok
  end

  def autocomplete
  	@keys = Dictionary.keys("*#{params[:filter]}*")
  	render json: @keys, status: :ok
  end
end
