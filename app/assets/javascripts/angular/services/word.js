angular.module('app').factory('WordService', function ($resource) {
  return $resource('/search/:word', {}, {
    show: { method: 'GET' , params: { id: '@word'}}
  });
});