var app = angular.module("app", ['ngRoute', 'ngResource', 'ngSanitize'])

app.config(function($routeProvider, $locationProvider){
   $routeProvider
   .when('/', {
      templateUrl : 'app/views/index.html',
      controller     : 'PrincipalCtrl',
      controllerAs: 'ctrl'
   })
   .otherwise ({ redirectTo: '/' });
});