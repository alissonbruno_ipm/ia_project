class Dictionary
	include ActiveModel::Serializers::JSON
	attr_accessor :word, :meaning
	def initialize word, meaning
		@word, @meaning = word, meaning
	end

	def attributes
    {word: nil, meaning: nil}
  end

	class << self
		def find_or_create word
			word.downcase!
			meaning = $REDIS.get(word)
			if meaning
				Dictionary.new(word, meaning)
			else
				create(word)
			end
		end

		def keys filter
			$REDIS.keys filter
		end

		private 
			def create word
				response = DictionaryApi.new.search(word)
				xml = Nokogiri::XML(response)
				definition = xml.at_xpath("//entry_list//entry//def")
				if definition
					meaning = definition.content.gsub(":", " ").capitalize
					$REDIS.set word, meaning
					Dictionary.new(word, meaning)
				end
			end
	end
end