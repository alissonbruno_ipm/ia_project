class DictionaryApi
	def search word
		url = URI.parse("http://www.dictionaryapi.com/api/v1/references/collegiate/xml/#{word}?key=22db59d8-2b84-4309-849e-50978735b453")
		req = Net::HTTP::Get.new(url.to_s)
		res = Net::HTTP.start(url.host, url.port) {|http|
		  http.request(req)
		}
		res.body
	end

end