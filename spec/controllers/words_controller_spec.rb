require 'rails_helper'

RSpec.describe WordsController, type: :controller do

  describe "GET #show" do
    it "retrieves a specific meaning" do
      get 'show', word: "blue", format: :json
      expect(response).to be_success 
      json = JSON.parse(response.body)
      expect(json['word']).to eq("blue")
      expect(json['meaning']).to include("of the color blue")
    end
  end

  describe "GET #autocomplete" do
    it "retrieves autocomplete words" do
      get 'autocomplete', filter: "bl", format: :json
      expect(response).to be_success
      expect(response.body).to include("blue")
    end
  end

end
